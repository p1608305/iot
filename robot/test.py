import time

import requests
import serial
from http.server import HTTPServer, BaseHTTPRequestHandler
import random

# ADDRESS = 'http://localhost:3000'
ADDRESS = 'https://192.168.78.78'

# configure the serial connections (the parameters differs on the device you are connecting to)
ser = serial.Serial(
    port='COM6',
    baudrate=9600,
    parity=serial.PARITY_ODD,
    stopbits=serial.STOPBITS_TWO,
    bytesize=serial.SEVENBITS
)

dict_action = {"avancer": 1, "droite": 2, "gauche": 22, "reculer": 4}

def getServeurAction():
    response = requests.get(ADDRESS + "/api/rover/instructions", verify=False)
    action = response.json()["action"]
    date = response.json()["time"]

    return dict_action[action], date


def processData(data):
    if "Distance" in data:
        data.replace("Distance : ", "")
        data.replace("cm", "")
        return float(data)

def main(test=False):
    last_date = 0
    close_serial = False
    ser.isOpen()
    count = 0

    if not test:
        while not close_serial:
            if ser.inWaiting() > 0:
                data = ser.readline()
                data = data.decode()
                data = data.replace("\r", "")
                data = data.replace("\n", "")
                if data == "No action":
                    distance = 800
                    action = None
                else:
                    data = data.split(" ")
                    distance = data[0]
                    action = data[1]
                distanceJson = {"distance": distance, "action": action}
                requests.put(ADDRESS + "/api/rover/distance", data=distanceJson, verify=False)
                # print('sent ' + distance)
            else:
                action, date = getServeurAction()
                if date > last_date or count <= 1:
                    ser.write(str.encode(str(action)))
                    time.sleep(4)
                    last_date = date
                    count += 1


if __name__ == '__main__':
    main()
x