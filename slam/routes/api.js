var express = require('express');
var router = express.Router();

const {RoverManager} = require("../models/RoverManager");
const {MapManager} = require("../models/MapManager");

/* GET Rover data. */
router.get('/rover', function(req, res, next) {
    res.status(200).json(RoverManager.instance.getData());
});

/* PUT Rover obstacle distance. */
router.put('/rover/distance', function(req, res, next) {
    let data = req.body['distance'];
    //console.log(req.body);
    //console.log('distance recue: ' + data);
    RoverManager.instance.updateDistance(data);
    res.status(200).send();
});

router.get('/rover/distance', function(req, res, next) {
    res.status(200).json(RoverManager.instance.lastDist);
});

/* PUT Rover orientation. */
router.put('/rover/orientation', function(req, res, next) {
    let data = req.body['orientation'];
    //console.log('orientation recue: ' + data);
    RoverManager.instance.updateOrientation(data);
    res.status(200).send();
});

/* POST Rover name. */
router.post('/rover/name', function(req, res, next) {
    RoverManager.instance.updateName("yoyo");
    res.status(200).send();
});

/* POST Rover action. */
router.post('/rover/action', function(req, res, next) {
    let data = req.body['action'];
    //console.log(req.body);
    //console.log('distance recue: ' + data);
    RoverManager.instance.noticeAction(data);
    res.status(200).send();
});

router.get('/map', function (req, res, next) {
    res.status(200).json(MapManager.instance.getMap());
});

router.get('/rover/instructions', function (req, res, next) {
    res.status(200).json(MapManager.instance.getLastInstructions());
});

router.post('/reset', function (req, res, next) {
    RoverManager.instance = new RoverManager();
    MapManager.instance = new MapManager();
    res.status(200).send();
});

module.exports = router;
