class GeoToolsIOT {
    /**
     * Calcule les coordonnées d'un point.
     * @param position [x,y]
     * @param orientation en RADIANS
     * @param distance en CM
     */
    static computeTargetPoint(position, orientation, distance) {
        let dx = distance * Math.cos(orientation);
        let dy = distance * Math.sin(orientation);
        let new_x = position[0] + dx;
        let new_y = position[1] + dy;
        return [new_x, new_y];
    }

    static toRad(angle) {
        if (angle > 180)
            angle -= 360;
        else if (angle < -180)
            angle += 360;
        let rad = angle * Math.PI / 180;

        angle -= 90;

        angle *= -1;

        //console.log('from ' + angle + ' to ' + rad);
        return rad;
    }
}

module.exports = { GeoToolsIOT };
