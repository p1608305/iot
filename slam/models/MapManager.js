const {RoverManager} = require("../models/RoverManager");
const {GeoToolsIOT} = require("../models/GeoToolsIOT");

const COLOR_UNDISCOVERED = 0;
const COLOR_CLEAR = 1;
const COLOR_WALL = 2;
const COLOR_ROBOT = 3;

const MAP_SIZE = 20;


class MapManager {
    static instance = new MapManager();

    constructor() {
        this.map = Array(MAP_SIZE * MAP_SIZE).fill(COLOR_UNDISCOVERED);    // 20x20 map
        this.roverIndex = -1;
        RoverManager.instance.position = [RoverManager.instance.step_len * MAP_SIZE / 2, RoverManager.instance.step_len * MAP_SIZE / 2];

        this.timer = setInterval(this.checkRoverChanges, 500);
        //clearInterval(this.timer);
    }

    getMap() {
        MapManager.instance.setBlockColor(RoverManager.instance.position, COLOR_ROBOT);

        return this.map;
    }

    getMapIndexFromPos(position) {
        let position2 = [
            Math.floor(position[0] / RoverManager.instance.step_len),
            Math.floor(position[1] / RoverManager.instance.step_len) ];

        return position2[1] + position2[0] * MAP_SIZE;
    }

    setBlockColor(position, color, rover=false) {
        let index = MapManager.instance.getMapIndexFromPos(position);
        if (index >= 0 && index < this.map.length) {
            this.map[index] = color;
            if (rover)
                RoverManager.instance.index = index;
        }
    }

    setMultipleBlocksColor(from, to, color) {
        let position2 = from;
        let distMax = Math.sqrt(Math.pow(from[0] - to[0], 2) + Math.pow(from[1] - to[1], 2));
        let curDist = 0;
        let step = RoverManager.instance.step_len;
        let vect = [to[0] - from[0], to[1] - from[1]];
        let angle = Math.atan2(vect[1], vect[0]);

        let done = false;
        while (!done) {
            curDist += step;
            let targetPos = GeoToolsIOT.computeTargetPoint(position2, angle, curDist);
            done = curDist >= distMax || this.map[MapManager.instance.getMapIndexFromPos(targetPos)];
            console.log('origin ' + from + ' to ' + to + ' cur dist is ' + curDist + ' max is ' + distMax + ' pos2 is ' + targetPos
            );
            if (!done)
                this.setBlockColor(targetPos, color);
        }
    }

    /**
     * Change un bloc en fonction de l'orientation du robot et de sa distance de perception
     */
    computeChanges() {
        const rover_step_len = RoverManager.instance.step_len;
        const maxDist = 80;    // le robot ne peut pas voir au delà de 4 cases
        let dist = RoverManager.instance.lastDist;
        if (dist > maxDist)
            dist = maxDist;
        const orient = GeoToolsIOT.toRad(RoverManager.instance.lastOrient);
        const position = RoverManager.instance.position;

        // calcul du point cible
        let target_position = GeoToolsIOT.computeTargetPoint(position, orient, dist);

        if (dist < maxDist)
            MapManager.instance.setBlockColor(target_position, COLOR_WALL);

        // les cases du robot à la target position sont CLEAR
        let tmpPos = RoverManager.instance.position;
        MapManager.instance.setMultipleBlocksColor(tmpPos, target_position, COLOR_CLEAR);
        console.log('done');
        MapManager.instance.setBlockColor(RoverManager.instance.position, COLOR_ROBOT, true);

        // l'ancienne case occupée par le robot est logiquement CLEAR
        if (RoverManager.instance.old_position !== null)
            MapManager.instance.setBlockColor(RoverManager.instance.old_position, COLOR_CLEAR);
    }

    checkRoverChanges() {
        let curTime = new Date().getTime() / 1000;
        if (curTime - RoverManager.instance.dateLastDist <= 3
            && curTime - RoverManager.instance.dateLastOrient <= 3) {
            MapManager.instance.computeChanges();
        } else {
            //console.log('Données d\'orientation et / ou de distance trop anciennes !');
        }
    }

    getLastInstructions() {
        /*let range = ['avancer', 'reculer', 'droite', 'gauche'];
        if (RoverManager.instance.lastActionDone) {
            RoverManager.instance.lastActionDone = false;
            if (RoverManager.instance.nextAction !== "reculer" && RoverManager.instance.nextAction !== "avancer")
                RoverManager.instance.nextAction = range[Math.floor(Math.random() * 1)];
            else
                RoverManager.instance.nextAction = range[Math.floor(1 + Math.random() * 2)];

            RoverManager.instance.nextActionTime = new Date().getTime() / 1000;
        }*/
        if (RoverManager.instance.lastActionDone) {
            RoverManager.instance.lastActionDone = false;
            let choices1 = []
            let choices2 = []
            let choice3 = []
            var final_choice = "None"
            const rovIndex = RoverManager.instance.index;
            const rovPos = RoverManager.instance.position;
            const rovOrient = GeoToolsIOT.toRad(RoverManager.instance.lastOrient);
            const dist = RoverManager.instance.step_len;
            let target_devant = GeoToolsIOT.computeTargetPoint(rovPos, rovOrient, dist);
            let target_gauche = GeoToolsIOT.computeTargetPoint(rovPos, rovOrient + Math.PI / 2, dist);
            let target_droite = GeoToolsIOT.computeTargetPoint(rovPos, rovOrient - Math.PI / 2, dist);

            let index_devant = MapManager.instance.getMapIndexFromPos(target_devant);
            let index_gauche = MapManager.instance.getMapIndexFromPos(target_gauche);
            let index_droite = MapManager.instance.getMapIndexFromPos(target_droite);

            // Mets dans un tableau toutes les cases qui n'ont pas d'obstacles
            if (this.map[index_devant] !== COLOR_WALL)
                choices1.push('avancer');
            if (this.map[index_gauche] !== COLOR_WALL)
                choices1.push('gauche');
            if (this.map[index_droite] !== COLOR_WALL)
                choices1.push('droite');

            console.log(choices1, "WALL")

            if (choices1.length > 0)
            {
                if (this.map[index_devant] !== COLOR_CLEAR && this.map[index_devant] !== COLOR_WALL)
                    choices2.push('avancer');
                if (this.map[index_gauche] !== COLOR_CLEAR && this.map[index_gauche] !== COLOR_WALL)
                    choices2.push('gauche');
                if (this.map[index_droite] !== COLOR_CLEAR && this.map[index_droite] !== COLOR_WALL)
                    choices2.push('droite');

                console.log(choices2, "ALL")
                if (choices2.length > 0)
                {
                    final_choice = choices2[Math.floor(Math.random() * choices2.length)];
                }
                else
                {
                    final_choice = choices1[Math.floor(Math.random() * choices1.length)];
                }
            }
            else {
                final_choice = "reculer";
            }
            RoverManager.instance.nextAction = final_choice;
            RoverManager.instance.nextActionTime = new Date().getTime() / 1000;
        }

        // + start & stop
        return {
            'action': RoverManager.instance.nextAction,
            'time': RoverManager.instance.nextActionTime
        }
    }
}

module.exports = { MapManager };
