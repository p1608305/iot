const {GeoToolsIOT} = require("../models/GeoToolsIOT");

class RoverManager {
    static instance = new RoverManager();

    constructor() {
        this.name = 'Rover-bogoss'
        this.lastDist = 0;
        this.dateLastDist = 0;
        this.lastOrient = 0;
        this.dateLastOrient = 0;
        this.step_len = 14;
        this.position = [0, 0];
        this.old_position = null;
        this.lastActionDone = true;
        this.nextAction = null;
        this.nextActionTime = null;
        this.index = -1;

        console.log('rover initied');
    }

    getData() {
        return {
            'name': this.name,
            'lastDist': this.lastDist,
            'dateLastDist': this.dateLastDist,
            'lastOrient': this.lastOrient,
            'dateLastOrient': this.dateLastOrient,
            'position': this.position
        };
    }

    updateName(name) {
        this.name = name;
    }

    updateDistance(distance) {
        this.lastDist = distance;
        this.lastActionDone = true;
        this.dateLastDist = new Date().getTime() / 1000;

        // la position du robot est changée ici
        let coefForward = 0
        if (this.nextAction === "reculer")
            coefForward = -Math.PI;

        if (this.nextAction === "reculer" || this.nextAction === "avancer") {
            this.old_position = this.position;
            this.position = GeoToolsIOT.computeTargetPoint(
                this.position,
                GeoToolsIOT.toRad(this.lastOrient) + coefForward,
                this.step_len);
        }
    }

    updateOrientation(orientation) {
        this.lastOrient = orientation;
        this.dateLastOrient = new Date().getTime() / 1000;
    }
}

module.exports = { RoverManager };