//const ADDRESS = 'http://localhost:3000';
const ADDRESS = 'https://192.168.78.78';

const mapCanvas = document.getElementById('divOrientation');
const btnStart = document.getElementById('btnStart');

let alpha = 0;
let timer;
let start = false;

async function sendOrientation(fake=null) {
    try {
        await fetch(`${ADDRESS}/api/rover/orientation`, {
            method: "PUT",
            headers: {
                "Content-Type": "application/json"
            },
            body: JSON.stringify({orientation: (fake === null ? alpha : fake)})
        })
            .then(function () {
                mapCanvas.innerText = `new orientation sent: ${alpha}`;
            });
    } catch (error) {
        console.error(error);
    }
}

async function sendDistance(fakeDistance) {
    try {
        await fetch(`${ADDRESS}/api/rover/distance`, {
            method: "PUT",
            headers: {
                "Content-Type": "application/json"
            },
            body: JSON.stringify({distance: fakeDistance})
        })
    } catch (error) {
        console.error(error);
    }
}

window.addEventListener('deviceorientation', (event) => {
    // console.log(`${event.alpha} : ${event.beta} : ${event.gamma}`);
    alpha = event.alpha;
});

function timerPutOrientation() {
    start = !start;
    console.log('click');
    if(start) {
        console.log('started');
        btnStart.textContent = "Stop";
        timer = setInterval(function () {
            sendOrientation();
        }, 500);
    }
    else {
        console.log('stopped');
        btnStart.textContent = "Start";
        clearInterval(timer);
    }
}


