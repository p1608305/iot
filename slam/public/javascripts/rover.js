//const ADDRESS = 'http://localhost:3000';
const ADDRESS = 'https://192.168.78.78';

let boot = false;
let timer;
let fakeOrientation = 0;

const mapCanvas = document.getElementById('mapCanvas');
let ctx = mapCanvas.getContext('2d');
const zoom = 20;

const symbolCanvas = document.getElementById('orientation_symbol');
let ctxSymbol = symbolCanvas.getContext('2d');

const btnBoot = document.getElementById('onOffBtn');
const propDiv = document.getElementById('propertiesPanel');
const instrDiv = document.getElementById('instructionsPanel');

const COLOR_UNDISCOVERED = 0;
const COLOR_CLEAR = 1;
const COLOR_WALL = 2;
const COLOR_ROBOT = 3;

function bootRover() {
    boot = !boot;
    console.log(boot ? 'start' : 'stop');
    timerUpdateResource(boot);
}

function computeTargetPoint(position, orientation, distance) {
    let dx = distance * Math.cos(orientation);
    let dy = distance * Math.sin(orientation);
    let new_x = position[0] + dx;
    let new_y = position[1] + dy;
    return [new_x, new_y];
}

function toRad(angle) {
    if (angle > 180)
        angle -= 360;
    else if (angle < -180)
        angle += 360;

    angle -= 90;

    angle *= -1;
    let rad = angle * Math.PI / 180;
    //console.log('from ' + angle + ' to ' + rad);
    return rad;
}

// Récupération des infos de la map
async function getMap() {
    //console.log(propDiv.innerText);
    let orientation = JSON.parse(propDiv.innerText)["lastOrient"];
    //orientation += 90;
    //let center = JSON.parse(propDiv.innerText)["position"];
    let center = [25,25];

    orientation = toRad(orientation);

    function fillMapCanvas(data) {
        let length = data.length;
        let width = Math.sqrt(length) * zoom;
        let height = Math.sqrt(length) * zoom;
        //console.log('width: ' + width / zoom);
        mapCanvas.setAttribute('width', width);
        mapCanvas.setAttribute('height', height);
        //console.log(data);

        let x = 0;
        let y = 0;

        let undiscoveredImage = new Image();
        undiscoveredImage.src = 'images/fog.gif';

        let clearImage = new Image();
        clearImage.src = 'images/clear.png';

        let wallImage = new Image();
        wallImage.src = 'images/cone.png';

        let robotImage = new Image();
        robotImage.src = 'images/robot.png';

        for (let i = 0; i < length; i++) {

            if (data[i] === COLOR_UNDISCOVERED) {
                ctx.drawImage(undiscoveredImage, x, y, zoom, zoom);
            } else if (data[i] === COLOR_CLEAR) {
                ctx.drawImage(clearImage, x, y, zoom, zoom);
            } else if (data[i] === COLOR_WALL) {
                ctx.drawImage(wallImage, x, y, zoom, zoom);
            } else if (data[i] === COLOR_ROBOT) {
                ctx.drawImage(robotImage, x, y, zoom, zoom);
            }

            x += zoom;
            if (x >= width) {
                x = 0;
                y += zoom;
            }
        }

        ctxSymbol.fillStyle = 'rgb(170,170,170)';
        ctxSymbol.strokeStyle = 'rgb(200,0,200)';
        ctxSymbol.fill();
        ctxSymbol.fillRect(0,0,50,50);


        let dist = 30;
        let dest = computeTargetPoint(center, orientation, dist);
        ctxSymbol.beginPath();
        ctxSymbol.moveTo(center[0], center[1]);
        ctxSymbol.lineTo(dest[0], dest[1]);
        ctxSymbol.stroke();
    }

    try {
        await fetch(`${ADDRESS}/api/map`, {
            method: "GET",
            headers: {
                "Content-Type": "application/json"
            }
        })
            .then(async function (response) {
                let data = await response.json();
                //console.log('Received a map with length ' + data.length)
                fillMapCanvas(data);
            });

    } catch (error) {
        console.error(error);
    }
}

// Récupération des infos du rover
async function getRoverInfos() {
    try {
        await fetch(`${ADDRESS}/api/rover`, {
            method: "GET",
            headers: {
                "Content-Type": "application/json"
            }
        })
            .then(async function (response) {
                let data = await response.text();
                //console.log('Received rover infos ' + data)
                propDiv.innerText = data;
            });

    } catch (error) {
        console.error(error);
    }
}

async function reset() {
    try {
        await fetch(`${ADDRESS}/api/reset`, {
            method: "POST",
        });
    } catch (error) {
        console.error(error);
    }
}

async function getInstructionsInfos() {
    try {
        await fetch(`${ADDRESS}/api/rover/instructions`, {
            method: "GET",
            headers: {
                "Content-Type": "application/json"
            }
        })
            .then(async function (response) {
                let data = await response.text();
                //console.log('Received rover infos ' + data)
                instrDiv.innerText = data;
            });

    } catch (error) {
        console.error(error);
    }
}

async function sendOrientation(fake) {
    try {
        await fetch(`${ADDRESS}/api/rover/orientation`, {
            method: "PUT",
            headers: {
                "Content-Type": "application/json"
            },
            body: JSON.stringify({orientation: fake})
        });
    } catch (error) {
        console.error(error);
    }
}

async function sendDistance(fakeDistance) {
    try {
        await fetch(`${ADDRESS}/api/rover/distance`, {
            method: "PUT",
            headers: {
                "Content-Type": "application/json"
            },
            body: JSON.stringify({distance: fakeDistance})
        })
    } catch (error) {
        console.error(error);
    }
}

async function doAction(event) {
    event.preventDefault();
    let action = JSON.parse(instrDiv.innerText)["action"];
    if (action === 'droite')
        fakeOrientation -= 90;
    else if (action === 'gauche')
        fakeOrientation += 90;

    while(fakeOrientation > 360)
        fakeOrientation -= 360;

    let fakeDistance = document.getElementById("fakeDistance").value;

    await sendOrientation(fakeOrientation);
    await sendDistance(fakeDistance);

    await getRoverInfos();
    await getInstructionsInfos();
}

function timerUpdateResource(start) {
    if(start) {
        btnBoot.innerText = 'Stop the simulation';
        timer = setInterval(function () {
            getMap();
            getRoverInfos();
            getInstructionsInfos();
        }, 1000);
    }
    else {
        btnBoot.innerText = 'Start the simulation';
        clearInterval(timer);
    }
}
