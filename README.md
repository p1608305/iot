# Projet IoT
Dépot de notre projet IoT.

## Présentation
Un agent robotique autonome intelligent explore un terrain inconnu.

## Matériel utilisé
- Robot mBot
- PC (pour la communication entre le robot et le serveur)
- Téléphone Android (pour la boussole)

## Application web
L'idée est que le robot envoie ses données à une application web qui les affichera afin d'avoir un système de cartographie.
Le robot envoie sa position et ce qu'il capte (un obstacle et la position estimée de l'obstacle).
La communication se fait à travers une API.
Une API externe est utilisée et nous sers à géolocaliser notre robot (dans l'hypothèse où ce dernier puisse se balader tout seul dans la rue...).

## Organisation
Nous travaillons de la manière suivante :
- 2 personnes sur la programmation de l'Arduino
- 3 personnes sur l'application web.

## Utilisation
Afin de pouvoir utiliser ce projet et le tester vous aurez besoin de Python 3 ainsi que de PySerial afin de mettre en place la communication. Vous aurez aussi besoin d'être connecté au réseau Wi-Fi eduroam ou d'utiliser le VPN de Lyon 1, ainsi que d'un téléphone sous Android pour la boussole.
Pour installer PySerial, exécutez la commande `pip install pyserial`.
Il faut ensuite exécuter les étapes suivantes :
- Branchez le robot au PC via le câble USB et allumez le robot
- Sur le téléphone Android rendez-vous à l'url https://192.168.78.78/static/mobile/ et posez le téléphone sur le robot
- Dans le gestionnaire de périphériques de votre PC récupérez le numéro du port USB
- Dans le script `test.py` renseignez le numéro du port série ligne 6 comme ceci : `port='/dev/COMX` en remplaçant X par le numéro du port (ex:  `port='/dev/COM6` si le robot est sur le port 6)
- Exécutez la commande `python ./robot/test.py` à la racine du dossier
- Dans votre navigateur rendez-vous à l'url https://192.168.78.78/static/
- Démarrez l'exécution de la cartographie
