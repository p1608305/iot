#include "MeOrion.h"

MeUltrasonicSensor ultraSensor(PORT_7); /* Ultrasonic module can ONLY be connected to port 3, 4, 6, 7, 8 of base shield. */
MeDCMotor leftMotor(M1); //Motor1 is Left Motor
MeDCMotor rightMotor(M2); //Motor2 is Right Motor
MeRGBLed led(0, 30);

const int LEFT_SPEED = 120;
const int RIGHT_SPEED = 120;


void forward()
{
  leftMotor.run(-LEFT_SPEED); //Motor1 (Left)  forward is -negative
  rightMotor.run(RIGHT_SPEED);  //Motor2 (Right) forward is +positive
  delay(1000);
  leftMotor.stop();
  rightMotor.stop();
}

void right()
{
  leftMotor.run(-LEFT_SPEED); //Motor1 (Left)  forward is -negative
  rightMotor.run(-RIGHT_SPEED);  //Motor2 (Right) forward is +positive
  delay(720);
  leftMotor.stop();
  rightMotor.stop();
}

void left()
{
  leftMotor.run(LEFT_SPEED); //Motor1 (Left)  forward is -negative
  rightMotor.run(RIGHT_SPEED);  //Motor2 (Right) forward is +positive
  delay(800);
  leftMotor.stop();
  rightMotor.stop();
}

void backward()
{
  leftMotor.run(LEFT_SPEED); //Motor1 (Left)  forward is -negative
  rightMotor.run(-RIGHT_SPEED);  //Motor2 (Right) forward is +positive
  delay(1000);
  leftMotor.stop();
  rightMotor.stop();
}

void printDistance(String msg) {
  double distance = ultraSensor.distanceCm();
  //Serial.print("Distance : ");
  if (distance < 20.00) {
    led.setColor(255, 0, 0); //Set both LED to Red
    led.show();
  }
  else {
    led.setColor(0, 255, 0); //Set both LED to Red
    led.show();
  }
  Serial.print(String(distance,2) + " " + msg + "\n");
  //Serial.println(" cm");
}

void setup()
{
  Serial.begin(9600);
  led.setpin(13);
}

void loop()
{
  // check if there is data available to read
  if (Serial.available() > 0) {
    // read a string of data
    String msg = Serial.readString();
    // print the data to the serial monitor
    //Serial.println("received msg : " + msg);

    if (msg == "1") {
      forward();
      printDistance("forward");
    }
    else if (msg == "2") {
      right();
      printDistance("right");
    }
    else if (msg == "22") {
      left();
      printDistance("left");
    }
    else if (msg == "4") {
      backward();
      printDistance("backward");
    }
    else {
      Serial.println("No action");
      delay(1000);
    }
  }

  delay(1000); // minimum time to read data again
}
